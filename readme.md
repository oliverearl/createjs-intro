# createjs-intro: My introduction to 2D game development using CreateJS

## Introduction
LinkedIn provided me with free access to a course on HTML5 game development using the [CreateJS](https://createjs.com) suite of libraries, so I wanted to make the most of it. This is my first introduction and end-product from following the directions in the course.

# How to use
Run npm install in order to install the required command line tools. (You need NPM / Node.js) - then run either *'npm run build-count'* for the Count game, or *'npm run build-rush'* for the Rush game. This takes care of Babel transcompilation and generates the necessary **game.js** files it is looking for. 

# Future Plans
In the future, I aim to develop my own basic games using these technologies to target both desktop and mobile markets.

# Course Content
The course can be found on LinkedIn Learning (formerly Lydia) [here](https://www.linkedin.com/learning/learning-html5-game-development). The content for the course can be found in the author's [GitHub repository.](https://github.com/makzan/html5-game-development-video-2nd-edition/tree/master)

# Additional
Software is for educational purposes only.

# Licensing
MIT.

Original course is Copyright &copy; 2016 Thomas Seng Hin Mak.