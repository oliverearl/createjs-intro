/**
 * Count Game
 * @author Oliver Earl <oliver@oliverearl.co.uk>
*/

class Game
{
    constructor()
    {
        console.log(`Game initialised. Version ${this.version()}`);

        this.canvas = document.getElementById("game-canvas");
        
        this.stage = new createjs.Stage(this.canvas);
        this.stage.width = this.canvas.width;
        this.stage.height = this.canvas.height;

        // enable mobile support - this enables tap handling
        createjs.Touch.enable(this.stage);

        // activate retina display support
        this.retinalise();

        // activate game data class to store current game state
        this.gameData = new GameData();

        //createjs.Ticker.setFPS(60); @deprecated
        createjs.Ticker.framerate = 60;
        
        // We want to re-draw the stage every frame, so we link this action to the ticker.
        createjs.Ticker.on("tick", this.stage);

        // Load sounds
        this.loadSound();

        this.restartGame();
    }

    generateMultipleBoxes(amount = 10)
    {
        // We add backwards due to Createjs stage hierarchy - think graphical layers
        for (let i = amount; i > 0; i--) {
            let movieClip = new NumberedBox(this, i);
            this.stage.addChild(movieClip);

            // random position
            movieClip.x = Math.random() * (this.stage.width - movieClip.getBounds().width);
            movieClip.y = Math.random() * (this.stage.height - movieClip.getBounds().height);
        }

    }

    handleClick(numberedBox)
    {
        if (this.gameData.isRightNumber(numberedBox.number)) {
            this.stage.removeChild(numberedBox);
            this.gameData.nextNumber();
        }

        // if game over
        if (this.gameData.isGameWin()) {
            let gameOverView = new lib.GameOverView();
            this.stage.addChild(gameOverView);
            createjs.Sound.play("Game Over");

            gameOverView.restartButton.on('click', (function() {
                createjs.Sound.play("Select");
                this.restartGame();
            }).bind(this));
        }
    }

    retinalise()
    {
        this.stage.width = this.canvas.width;
        this.stage.height = this.canvas.height;

        let ratio = window.devicePixelRatio;
        if (ratio === undefined) {
            // abort if not a retina display
            return;
        }

        this.canvas.setAttribute('width', Math.round(this.stage.width * ratio));
        this.canvas.setAttribute('height', Math.round(this.stage.height * ratio));
        this.stage.scaleX = this.stage.scaleY = ratio;

        // set CSS
        this.canvas.style.width = this.stage.width + "px";
        this.canvas.style.height = this.stage.height + "px";
    }

    restartGame()
    {
        // Initialise
        this.gameData.resetData();
        this.stage.removeAllChildren();

        // Render background and boxes
        this.stage.addChild(new lib.Background());
        this.generateMultipleBoxes(this.gameData.amountOfBoxes);

        console.log('Game loaded.');
    }

    loadSound()
    {
        createjs.Sound.registerSound("soundfx/blip.aiff", "Select");
        createjs.Sound.registerSound("soundfx/gameover.aiff", "Game Over");
        createjs.Sound.alternativeExtensions = ["ogg", "wav"];
    }

    version()
    {
        return '1.0.0';
    } 
}

class NumberedBox extends createjs.Container
{
    constructor(game, number = 0)
    {
        super();

        this.game = game;
        this.number = number;

        let movieClip = new lib.NumberedBox();
        movieClip.numberText.text = number;
        movieClip.numberText.font = "28px Oswald";

       // new createjs.ButtonHelper(movieClip, 0, 1, 2, false, new lib.NumberedBox(), 3);

        this.addChild(movieClip);
        this.setBounds(0, 0, 50, 50);

        // handle click/tap behaviour
        this.on('click', this.handleClick.bind(this));
    }

    handleClick()
    {
        this.game.handleClick(this);
        createjs.Sound.play("Select");
    }
}

class GameData
{
    constructor()
    {
        this.amountOfBoxes = 10;
        this.resetData();
    }

    resetData()
    {
        this.currentNumber = 1;
    }

    nextNumber()
    {
        this.currentNumber += 1;
    }

    isRightNumber(number)
    {
        return (number === this.currentNumber);
    }

    isGameWin()
    {
        return (this.currentNumber > this.amountOfBoxes);
    }
}

let game = new Game();